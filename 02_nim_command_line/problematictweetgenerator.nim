proc print[T](texts: varargs[T, `$`]) =
    for x in texts: write(stdout, x)
    echo ""

proc printnol(text: string) = write(stdout, text)

proc input(): string = readLine(stdin)

proc haseul()=
    print ("")
    print ("###################################################")
    print ("#                                                 #")
    print ("# The problematic tweet generator - by Kpopalypse #")
    print ("#                                                 #")
    print ("###################################################")
    print ("")
    print ("Name a k-pop performer that crazy stans will shield for, " &
        "no matter what, and press ENTER.")
    print ("")
    printnol ("> ")
    var oliviahye = input()
    print ("")
    print ("Name the group that the k-pop performer belongs to, and press ENTER.")
    print ("If the k-pop performer is a solo performer, just press ENTER.")
    print ("")
    printnol ("> ")
    var gowon = input()
    if gowon == (""):
        gowon = oliviahye
    print ("")
    print ("Is your k-pop performer male or female? (m/f)")
    print ("")
    var
        yves = input()
        pronoun: string
        pronoun2: string
        pronoun3: string

    if yves[0] == 'M' or yves[0] == 'm':
        pronoun = "he"
        pronoun2 = "his"
        pronoun3 = "him"
    elif yves[0] == 'F' or yves[0] == 'f':
        pronoun = "she"
        pronoun2 = "her"
        pronoun3 = "her"
    elif yves[0] == 'a' or yves[0] == 'A' or yves[0] == 'h' or yves[0] == 'H':
        pronoun = "ze"
        pronoun2 = "zer"
        pronoun3 = "zim"
    else:
        print ("")
        print ("I'm sorry, gender ", yves,
            " is not supported by this program at this time.  Let's start again.")
        print ("")
        haseul()
    print ("Select a very unfunny thing that you should never joke about under any circumstances from the following list, and press ENTER.")
    print ("")
    print ("Press 1 for the New Zealand shooting (the original)")
    print ("Press 2 for the Jewish Holocaust")
    print ("Press 3 for the atomic bombs dropped on Japan")
    print ("Press 4 for slavery in America")
    print ("Press 5 for the attacks on the World Trade Center")
    print ("Press 6 for the Chernobyl meltdown")
    print ("Press 7 for the New Zealand shooting (the remix)")
    print ("Press 8 for the KKK")
    print ("Press 9 for neo-Nazis")
    print ("Press 10 for child pornography")
    print ("Press 11 for the Sandy Hook shooting")
    print ("Press 12 for rape")
    print ("Press 99 for Kpopalypse blog")
    printnol ("> ")
    var
        chuu = input()
        tragicincident: string
        problematicjoke: string
    if chuu == ("1"):
        tragicincident = "did the New Zealand shooting"
        problematicjoke = (pronoun & " didn't know bullets hurt people")
    elif chuu == ("2"):
        tragicincident = "was found to have run a Nazi death camp"
        problematicjoke = (pronoun & " just thought " & pronoun & " was lighting the gas pilot light for the kitchen")
    elif chuu == ("3"):
        tragicincident = "dropped atomic bombs on Japan"
        problematicjoke = (pronoun2 & " finger just slipped and hit the red button by accident")
    elif chuu == ("4"):
        tragicincident = "was alive in America's early days and kept slaves"
        problematicjoke = (pronoun & " was going to pay them when " & pronoun2 & " contract expired")
    elif chuu == ("5"):
        tragicincident = "flew a plane into the World Trade Center"
        problematicjoke = (pronoun & " was just looking for the runway with " & gowon & " written on it but ran out of fuel")
    elif chuu == ("6"):
        tragicincident = "was an engineer at the Chernobyl nuclear plant when it melted down"
        problematicjoke = (pronoun & " just was told to make the area glow the right shade of green for " & pronoun2 & " next video shoot")
    elif chuu == ("7"):
        tragicincident = "did the New Zealand shooting"
        problematicjoke = (pronoun & " was just testing the guns' safety features and they all accidentally went off")
    elif chuu == ("8"):
        tragicincident = "was found wearing a KKK robe"
        problematicjoke = (pronoun & " just wanted to hide " & pronoun2 & " identitiy so " & pronoun2 & " millions of adoring fans would give " & pronoun3 & " some space")
    elif chuu == ("9"):
        tragicincident = "joined a neo-Nazi organisation"
        problematicjoke = (pronoun & " was just researching how neo-Nazis think so " & pronoun & " could write lyrics for " & pronoun2 & " next anti-racist comeback masterpiece")
    elif chuu == ("10"):
        tragicincident = "was found with child pornography"
        problematicjoke = (pronoun & " was just collecting lots of it so " & pronoun & " could hand it all into the police at once")
    elif chuu == ("11"):
        tragicincident = "did the Sandy Hook shooting"
        problematicjoke = (pronoun & " was told that the guns all fired candy, " & pronoun & " was just trying to give candy to the children")
    elif chuu == ("12"):
        tragicincident = "was caught raping someone"
        problematicjoke = ("it's impossible for " & oliviahye & " to be guilty, everybody would consent to " & pronoun3)
    elif chuu == ("99"):
        tragicincident = "was found to write for Kpopalypse blog"
        problematicjoke = ("actually that's completely unacceptable - " & pronoun & "'s cancelled and dismissed!")
    else:
        print ("")
        print ("Invalid reponse - gosh you really are failing at life today ",
            "aren't you.  Let's start again.")
        print ("")
        haseul()

    print ("")
    print ("Your problematic tweet is below, copy and paste as needed!")
    print ("")
    print ("--------------------------")
    print ("")
    print ("If " & oliviahye & " " & tragicincident & ", " & gowon & " stans would be like \"" & problematicjoke & "\".")
    print ("")
    print ("--------------------------")
    print ("")
    print ("Run the program again? (y/n)")
    print ("")
    printnol ("> ")
    var hyunjin = input()
    if hyunjin == ("y"):
        haseul()
    else:
        quit()

haseul()

#  vim: set ts=4 sts=4 sw=4 tw=0 et :
