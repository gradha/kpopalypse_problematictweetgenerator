import strutils, macros

include karax / prelude

type
  State = enum
    sStart, sQuantity, sSex, sExcuse, sResult

const
  blank = "_blank"
  inputFieldId = "inputFieldId"
  url_kpopalypse = "https://kpopalypse.com/2019/03/28/the-kpopalypse-problematic-tweet-generator/"
  url_source = "https://gitlab.com/gradha/kpopalypse_problematictweetgenerator"

var
  history: seq[kstring] = @[]
  screen = sStart
  errorMessage = kstring""
  oliviahye: kstring
  gowon: kstring
  pronoun: kstring
  pronoun2: kstring
  pronoun3: kstring
  tragicincident: kstring
  problematicjoke: kstring


proc toLower(x: kstring): kstring =
  let text = $x
  result = kstring(text.toLower)


proc generateResult(): kstring =
  result = ("If " & oliviahye & " " & tragicincident & ", " & gowon &
    " stans would be like \"" & problematicjoke & "\".")


proc getInput(): kstring =
  let inputField = getVNodeById(inputFieldId)
  if inputField.isNil or inputField.text == "":
    errorMessage = "Please type something"
    result = ""
  else:
    errorMessage = ""
    result = inputField.text
    inputField.setInputText ""


proc setOliviaHye() =
  let input = getInput()
  if input.len > 0:
    history.add input
    oliviahye = input
    screen = sQuantity


proc setGowon() =
  let input = getInput()
  if input.len > 0: history.add input
  else: history.add "Jennie style"
  errorMessage = ""
  gowon = input
  screen = sSex


# https://stackoverflow.com/a/33928558/172690
proc copyResultToClipboard() =
  # Prepare a variable with the contents of the whole result.
  var fullText = generateResult()

  # Use the variable fullText inside the generated javascript code.
  {.emit: """
if (window.clipboardData && window.clipboardData.setData) {
  // IE specific code path to prevent textarea being shown while dialog is visible.
  clipboardData.setData("Text", `fullText`);

} else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
  var textarea = document.createElement("textarea");
  textarea.textContent = `fullText`;
  textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
  document.body.appendChild(textarea);
  textarea.select();
  try {
    document.execCommand("copy");  // Security exception may be thrown by some browsers.
    alert("Text copied, now go paste it somewhere!");
  } catch (ex) {
    alert("Copy to clipboard failed :-(", ex);
  } finally {
    document.body.removeChild(textarea);
  }
}
""".}


# Generates the final output of the crazy comment generator. This calls the
# generateResult() proc we declared above and translates each line to an HTML
# div or p DOM element.
proc buildResult(): VNode =
  result = buildHtml(tdiv):
    p: discard
    tdiv:
      text """Your problematic tweet is below, copy and paste as needed!"""
    p: discard
    button:
      text "Copy text below to clipboard"
      proc onclick(ev: Event; n: VNode) =
        copyResultToClipboard()
    text "(if you don't see a success popup you will need to copy it manually)"
    hr: discard
    tdiv: text generateResult()
    p: discard
    a(href="https://twitter.com/share?ref_src=twsrc%5Etfw",
        class="twitter-share-button", data-size="large",
        data-text=generateResult(), data-show-count="false", target = blank):
      text "Tweet"
    script(async = "", src="https://platform.twitter.com/widgets.js",
        charset="utf-8"):
      discard


proc buildOliviaHye(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text """Name a k-pop performer that crazy stans will shield for,
no matter what, and press ENTER."""
    input(class = "input", id = inputFieldId, onkeyupenter = setOliviaHye)
    tdiv: text errorMessage


proc buildGowon(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text """Name the group that the k-pop performer belongs to,
      and press ENTER."""
    p:
      tdiv: text "If the k-pop performer is a solo performer, just press ENTER."
    input(class = "input", id = inputFieldId, onkeyupenter = setGowon)
    tdiv: text errorMessage


proc buildYves(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text "Select the sex of your k-pop performer: "
    button:
      text "male"
      proc onclick(ev: Event; n: VNode) =
        history.add "male"
        pronoun = "he"
        pronoun2 = "his"
        pronoun3 = "him"
        screen = sExcuse
    button:
      text "female"
      proc onclick(ev: Event; n: VNode) =
        history.add "female"
        pronoun = "she"
        pronoun2 = "her"
        pronoun3 = "her"
        screen = sExcuse
    button:
      text "???"
      proc onclick(ev: Event; n: VNode) =
        history.add "???"
        pronoun = "ze"
        pronoun2 = "zer"
        pronoun3 = "zim"
        screen = sExcuse
    tdiv: text errorMessage


proc buildChuu(): VNode =
  result = buildHtml(tdiv):
    p:
      tdiv: text """Select a very unfunny thing that you should never joke
      about under any circumstances from the following list."""
    button:
      text "New Zealand shooting (the original)"
      proc onclick(ev: Event; n: VNode) =
        history.add "New Zealand shooting (the original)"
        tragicincident = "did the New Zealand shooting"
        problematicjoke = (pronoun & " didn't know bullets hurt people")
        screen = sResult
    button:
      text "Jewish Holocaust"
      proc onclick(ev: Event; n: VNode) =
        history.add "Jewish Holocaust"
        tragicincident = "was found to have run a Nazi death camp"
        problematicjoke = (pronoun & " just thought " & pronoun &
          " was lighting the gas pilot light for the kitchen")
        screen = sResult
    button:
      text "atomic bombs dropped on Japan"
      proc onclick(ev: Event; n: VNode) =
        history.add "atomic bombs dropped on Japan"
        tragicincident = "dropped atomic bombs on Japan"
        problematicjoke = (pronoun2 &
          " finger just slipped and hit the red button by accident")
        screen = sResult
    button:
      text "slavery in America"
      proc onclick(ev: Event; n: VNode) =
        history.add "slavery in America"
        tragicincident = "was alive in America's early days and kept slaves"
        problematicjoke = (pronoun & " was going to pay them when " &
          pronoun2 & " contract expired")
        screen = sResult
    button:
      text "attacks on the World Trade Center"
      proc onclick(ev: Event; n: VNode) =
        history.add "attacks on the World Trade Center"
        tragicincident = "flew a plane into the World Trade Center"
        problematicjoke = (pronoun & " was just looking for the runway with " &
          gowon & " written on it but ran out of fuel")
        screen = sResult
    button:
      text "Chernobyl meltdown"
      proc onclick(ev: Event; n: VNode) =
        history.add "Chernobyl meltdown"
        tragicincident = "was an engineer at the Chernobyl nuclear plant when it melted down"
        problematicjoke = (pronoun &
          " just was told to make the area glow the right shade of green for " &
          pronoun2 & " next video shoot")
        screen = sResult
    button:
      text "New Zealand shooting (the remix)"
      proc onclick(ev: Event; n: VNode) =
        history.add "New Zealand shooting (the remix)"
        tragicincident = "did the New Zealand shooting"
        problematicjoke = (pronoun &
          " was just testing the guns' safety features and they all accidentally went off")
        screen = sResult
    button:
      text "KKK"
      proc onclick(ev: Event; n: VNode) =
        history.add "KKK"
        tragicincident = "was found wearing a KKK robe"
        problematicjoke = (pronoun & " just wanted to hide " & pronoun2 &
          " identitiy so " & pronoun2 &
          " millions of adoring fans would give " & pronoun3 & " some space")
        screen = sResult
    button:
      text "neo-Nazis"
      proc onclick(ev: Event; n: VNode) =
        history.add "neo-Nazis"
        tragicincident = "joined a neo-Nazi organisation"
        problematicjoke = (pronoun &
          " was just researching how neo-Nazis think so " & pronoun &
          " could write lyrics for " & pronoun2 &
          " next anti-racist comeback masterpiece")
        screen = sResult
    button:
      text "child pornography"
      proc onclick(ev: Event; n: VNode) =
        history.add "child pornography"
        tragicincident = "was found with child pornography"
        problematicjoke = (pronoun & " was just collecting lots of it so " &
          pronoun & " could hand it all into the police at once")
        screen = sResult
    button:
      text "Sandy Hook shooting"
      proc onclick(ev: Event; n: VNode) =
        history.add "Sandy Hook shooting"
        tragicincident = "did the Sandy Hook shooting"
        problematicjoke = (pronoun &
          " was told that the guns all fired candy, " & pronoun &
          " was just trying to give candy to the children")
        screen = sResult
    button:
      text "rape"
      proc onclick(ev: Event; n: VNode) =
        history.add "rape"
        tragicincident = "was caught raping someone"
        problematicjoke = ("it's impossible for " & oliviahye &
          " to be guilty, everybody would consent to " & pronoun3)
        screen = sResult
    button:
      text "Kpopalypse blog"
      proc onclick(ev: Event; n: VNode) =
        history.add "Kpopalypse blog"
        tragicincident = "was found to write for Kpopalypse blog"
        problematicjoke = ("actually that's completely unacceptable - " &
          pronoun & "'s cancelled and dismissed!")
        screen = sResult


proc resetState() =
  screen = sStart
  history = @[]

proc reloadPage() =
  {.emit: """location = location""".}

proc haseul(): VNode =
  result = buildHtml(tdiv):
    h1:
      text "The problematic tweet generator - by Kpopalypse"
    tdiv:
      text "For context on the original problematic tweet generator "
      a(target = blank, href = url_kpopalypse):
        text "follow this link"
      text ". Source code "
      a(targat = blank, href = url_source):
        text "here"
      text "."

    if history.len > 0:
      p: discard
      button:
        text "Click to reflect and return with a more mature image"
        proc onclick(ev: Event; n: VNode) =
          if screen == sResult:
            reloadPage()
          else:
            resetState()

    p: discard
    tdiv:
      for x in history:
        text x & ". "

    case screen:
      of sStart: buildOliviaHye()
      of sQuantity: buildGowon()
      of sSex: buildYves()
      of sExcuse: buildChuu()
      of sResult: buildResult()


setRenderer haseul
