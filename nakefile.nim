import bb_nake

const
  src_dir = "03_nim_webapp"
  base_filename = "problematictweetgenerator"
  nim_filename = base_filename & ".nim"
  js_filename = base_filename & ".js"
  pre_html = "public"/"template_start.html"
  post_html = "public"/"template_end.html"
  target_file = "public"/"index.html"


proc gen_web() =
  var buf = pre_html.read_file

  src_dir.with_dir:
    dire_shell("nim js -d:release", nim_filename)
    buf.add read_file(nimcache_dir/js_filename)

  buf.add post_html.read_file

  target_file.write_file(buf)


task defaultTask, "Generates web.": gen_web()
